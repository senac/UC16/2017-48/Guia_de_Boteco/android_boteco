package guiadeboteco.br.com.guiadeboteco.guiadeboteco;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {

    private Button entrar;
    private Button cancelar;
    private EditText usuario;
    private EditText senha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        entrar = findViewById(R.id.btentrarID);
        cancelar = findViewById(R.id.btcancelarID);
        usuario = findViewById(R.id.usuarioID);
        senha = findViewById(R.id.senhaID);

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }


        });

        entrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(usuario.getText().toString().equals("Admin")&& senha.getText().toString().equals("admin")){
                    startActivity(new Intent(LoginActivity.this,BotecoActivity.class));
                    Toast.makeText(getApplicationContext(), "Bem vindo ao Guia de Boteco", Toast.LENGTH_SHORT).show();


                }else {
                    Toast.makeText(getApplicationContext(),"usuario ou senha não confere", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


}
